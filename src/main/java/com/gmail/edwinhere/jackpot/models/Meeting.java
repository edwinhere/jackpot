package com.gmail.edwinhere.jackpot.models;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author Edwin Jose Palathinkal <edwinhere@gmail.com>
 */
@Data
public class Meeting {
	private Date date;
	private String venue;
	private String href;
}
