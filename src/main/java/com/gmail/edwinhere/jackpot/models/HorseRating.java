package com.gmail.edwinhere.jackpot.models;

import java.util.Date;
import lombok.Data;

/**
 *
 * @author Edwin Jose Palathinkal <edwinhere@gmail.com>
 */
@Data
public class HorseRating {
	private Date raceDate;
	private String horseClass;
	private String horseName;
	private int changeOfRating;
	private int rating;
}
