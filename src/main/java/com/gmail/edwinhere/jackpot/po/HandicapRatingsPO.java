package com.gmail.edwinhere.jackpot.po;

import com.gmail.edwinhere.jackpot.models.HorseRating;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Edwin Jose Palathinkal <edwinhere@gmail.com>
 */
public class HandicapRatingsPO extends PO {

	@FindBy(className = "STC_DDL")
	private WebElement raceDateCombo;

	@FindBy(css = ".xl70 font")
	private List<WebElement> classes;

	private WebDriver driver;

	public HandicapRatingsPO(WebDriver driver) {
		this.driver = driver;
	}
	
	public List<Date> getAllRaceDates() {
		final Select selectBox = new Select(raceDateCombo);
		List<WebElement> options = selectBox.getOptions();
		return options.stream()
			.map((o) -> o.getText())
			.map((s) -> toDate(s))
			.collect(Collectors.toList());
	}

	public HashMap<Date, List<HorseRating>> getAllRatings() {
		final Select selectBox = new Select(raceDateCombo);
		List<Date> options = getAllRaceDates();

		int index = 0;
		for(Date option : options) {
			selectBox.selectByIndex(index);
			WebDriverWait wait = new WebDriverWait(driver, index);
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					WebElement shownRaceDate = driver.findElement(By.cssSelector(""));
					String[] split = shownRaceDate.getText().split(" ");
					String currentDateString = split[split.length - 1].trim();
					Date raceDate = toDate(currentDateString);
					return raceDate == option;
				}
			});
			index++;
		}
		return null;
	}
}
