package com.gmail.edwinhere.jackpot.po;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.OptionalInt;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Edwin Jose Palathinkal <edwinhere@gmail.com>
 */
public class PO {

	public boolean isElementPresent(WebElement element, By by) {
		try {
			element.findElement(by);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public List<List<Pair<String, String>>> getTableWithLinks(WebElement table) {
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		return rows.stream()
						.map((r) -> r.findElements(By.xpath("./td")))
						.map((cs) -> cs.stream()
						.map((c) -> {
							String href = "";
							if (isElementPresent(c, By.xpath("./a"))) {
								WebElement innerLink = c.findElement(By.xpath("./a"));
								href = innerLink.getAttribute("href");
							}
							return Pair.of(c.getText().trim().toUpperCase(), href);
						}).collect(Collectors.toList()))
						.collect(Collectors.toList());
	}

	public List<List<String>> getTable(WebElement table) {
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		return rows.stream()
						.map((r) -> r.findElements(By.xpath("./td")))
						.map((cs) -> cs.stream()
						.map((c) -> c.getText()).collect(Collectors.toList()))
						.collect(Collectors.toList());
	}

	public Pair<Integer, Integer> getTableSize(WebElement table) {
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		OptionalInt max = rows.stream()
						.map((e) -> e.findElements(By.xpath("./td")))
						.mapToInt((es) -> es.size())
						.max();
		return Pair.of(rows.size(), max.orElse(0));
	}

	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	public Date toDate(String dateString) {
		Date result = null;
		try {
			result = dateFormat.parse(dateString);
		} catch (ParseException ex) {
			Logger.getLogger(HandicapsPO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return result;
	}

}
