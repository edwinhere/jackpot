package com.gmail.edwinhere.jackpot.po;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import com.gmail.edwinhere.jackpot.models.Meeting;

/**
 *
 * @author Edwin Jose Palathinkal <edwinhere@gmail.com>
 */
public class HandicapsPO extends PO {

	@FindBy(className = "STC_Gdv_WP")
	@CacheLookup
	private WebElement handicapsTable;

	public List<Meeting> getMeetings() {
		return getTableWithLinks(handicapsTable).stream()
						.skip(1)
						.map((pairs) -> {
							Meeting m = new Meeting();
							m.setDate(super.toDate(pairs.get(0).getLeft()));
							m.setVenue(pairs.get(1).getLeft());
							m.setHref(pairs.get(0).getRight());
							return m;
						}).collect(Collectors.toList());
	}
}
